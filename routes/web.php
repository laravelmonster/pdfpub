<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Home@main');
Route::post('login', 'User@login')->name('login');
Route::get('login', 'Home@main')->name('login');
Route::get('forgot-password', 'User@forgotPassword')->name('user.forgot-password');
Route::post('forgot-password', 'User@resetPassword')->name('user.reset-password');

Route::resource('user', 'User');
Route::group(['middleware' => 'auth'], function () {
    Route::get('change-password', 'User@changePassword')->name('change-password');
    Route::post('change-password', 'User@updatePassword')->name('change-password');
    Route::get('recharge', 'Recharge@request')->name("recharge");
    Route::get('digest', 'User@digest')->name('digest');
    Route::delete('logout', 'User@logout')->name('user.logout');
    Route::resource('key', 'Key');
    Route::resource('pdf', 'WebPdf');
    Route::get('logout', 'User@logout')->name('logout');
});
Route::get('user/create','User@create')->name("user.create");
