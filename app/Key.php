<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Key extends Model
{
    public $fillable = ['key','domain'];

    public function pdfs(){
        return $this->hasMany(KeyPdf::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
