<?php
function active($url, $default = 'active current text-primary')
{
    return $url == url()->current() ? $default : '';
}