<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Response;

class KeyPdf extends Model
{
    use SoftDeletes;
    public $fillable = ['name', 'key_id'];

    public static function print($folder, $file)
    {
        $storage_path = storage_path('app/' . $folder . '/' . $file);
        if (file_exists($storage_path))
            return new Response(file_get_contents($storage_path), 200, array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'inline; filename="' . $file . '"',
            ));
        return back()->with('message', 'File has already been deleted');
    }

    public static function download($folder, $file)
    {
        $storage_path = storage_path('app/' . $folder . '/' . $file);
        if (file_exists($storage_path))
            return new Response(file_get_contents($storage_path), 200, array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename="' . $file . '"',
            ));
        return back()->with('message', 'File has already been deleted');

    }

    public function key()
    {
        return $this->belongsTo(Key::class);
    }
}
