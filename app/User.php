<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @property mixed $password
 * @property mixed $keys
 * @property mixed $quota
 * @property mixed $attributes
 * @property mixed $created_at
 * @property mixed $id
 * @property mixed $updated_at
 */
class User extends Authenticatable
{

    public $fillable = ['user', 'email', 'password'];

    /**
     * @param $password
     * @return mixed
     */
    public function setPasswordAttribute($password)
    {
        return $this->attributes['password'] = bcrypt($password);
    }

    public function keys()
    {
        return $this->hasMany(Key::class);
    }

    public function quota()
    {
        return $this->hasOne(Quota::class);
    }
}
