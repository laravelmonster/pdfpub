<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quota extends Model
{
    public $fillable = ['quota','user_id'];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
