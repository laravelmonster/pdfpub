<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePdf;
use App\KeyPdf;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use LaravelMonster\Wordtopdf\Converter;
use PDF;
use App\Key;

class PdfController extends Controller
{
    use Helpers;

    /**
     * Display a listing of the resource.
     *
     * @param CreatePdf $request
     * @param $key
     * @return Response
     */
    public function index(CreatePdf $request, $key)
    {

        $key = Key::where('key', $key)->first();
        $quota = $key->user->quota;
        if ($quota->quota < 0) {
            $this->response->errorForbidden('You have exceeded your quota limit');
        }
        $quota->decrement('quota');
        $quota->save();

        $pdf = false;
        $noExtensionName = str_replace('/', '', str_replace('.', '', $request->get('name', substr(str_replace('.', '', bcrypt($request->fullUrl())), 10, 15))));
        $name = $noExtensionName . '.pdf';
        $path = '/app/'.$request->route('key') . '/' . $name;
        $filename = storage_path($path);
        if ($request->get('cache', false) && file_exists($filename)) {
            return new Response(file_get_contents($filename), 200, array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename="' . $name . '"'
            ));
        }
        if ($request->get('url')) {
            $pdf = $this->createFromUrl($request);
        } else if ($request->get('html')) {
            $message = 'You did not pass any html to us. Please pass html variables.';
            $html = $request->get('html', $message);
            $pdf = PDF::loadHTML(strlen($html) > 0 ? $html : $message);
        } else if ($request->file('doc', $request->file('word', false))) {
            $pdf = (new Converter())->convert($request, $noExtensionName);
        }

        if (!$pdf) {
            $quota->increment('quota');
            $quota->save();
            $this->response->errorBadRequest();
        }


        if ($request->get('cache', false)) {
            $key->pdfs()->save(new KeyPdf(['name' => $name]));
            $pdf->save($filename);
        }


        return $pdf->download($name);
    }

    /**
     * @param CreatePdf $request
     * @return PDF
     */
    public function createFromUrl(CreatePdf $request)
    {
        return PDF::loadFile($request->get('url'));
    }

}
