<?php

namespace App\Http\Controllers;

use App\KeyPdf;
use Illuminate\Http\Request;

class WebPdf extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $pdf = KeyPdf::find($id);
        $valid = $request->user()->keys()->where('id', $pdf->key_id)->firstOrFail();
        return KeyPdf::{$request->get('type','print')}($valid->key, $pdf->name);
    }


    public function destroy(Request $request, $id){
        $orFail = KeyPdf::findOrFail($id);
        if($orFail->key()->where('user_id',$request->user()->id)->firstOrFail()){
            $orFail->delete();
            return back()->with('message','PDF has successfully deleted');
        }
        return back();
    }

}
