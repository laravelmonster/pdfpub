<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Recharge extends Controller
{
    public function request()
    {
        return view('recharge.index');
    }

    public function recharge()
    {
        return back()->with('message','Quota Updated');
    }
}
