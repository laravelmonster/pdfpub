<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Home extends Controller
{
    public function main(){
    	if(!auth()->check())
    		return view('auth.login');
    	return view('user.dashboard');
    }

   
}
