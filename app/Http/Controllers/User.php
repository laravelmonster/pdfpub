<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddUser;
use App\Events\UserCreated;
use App\Quota;
use App\User as Customer;
use Illuminate\Http\Request;

class User extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('auth.signup');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AddUser|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddUser $request)
    {
        $user = Customer::forceCreate($request->only(['email', 'password', 'username']));
        $user->quota()->save(new Quota(['quota' => 10]));
        event(new UserCreated($user));
        auth()->logout();
        return redirect('/')->with('message', 'User has been successfully created');
    }


    public function login(Request $request)
    {
        if (auth()->attempt($request->only('username', 'password'))) {
            $quota = auth()->user()->quota;
            if ($quota && $quota->quota < 5) {
                $quota->increment('quota');
                $quota->save();
            }
            return redirect()->intended('/');
        }
        return back()->with('message', 'Login Denied.')->with("type", 'danger');
    }

    public function forgotPassword()
    {
        return view('auth.forgot-password');
    }

    public function resetPassword()
    {
        auth()->logout();
        return redirect('/')->with('message', 'Password has been reset and email has been sent with new password.');
    }

    public function logout()
    {
        auth()->logout();
        return back();
    }

    public function changePassword()
    {
        return view('user.change-password');
    }

    public function updatePassword(\App\Http\Requests\UpdatePassword $request)
    {
        if (auth()->attempt(['username' => $request->user()->username, 'password' => $request->old_password])) {
            $request->user()->password = $request->password;
            $request->user()->save();
            return back()->with('message', 'Password has been changed successfully');
        }
        return back()->with('message', 'Could not change the password')->with('type', 'danger');
    }

    public function digest()
    {
        return view('user.digest');
    }
}
