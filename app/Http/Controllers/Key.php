<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Key as KeyCreator;

class Key extends Controller
{

    public function create(){
        return view('key.create');
    }
    public function index(){
        return view('key.lists');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $message = 'Password was invalid';
        if(auth()->attempt(array_merge($request->only('password'),['username'=>auth()->user()->username]))){
            $request->user()->keys()->save(new KeyCreator(['key'=>str_random(15)]));
            $message = "Key has been successfully created.";
        }
        return redirect(route("key.index"))->with("message",$message);
    }

    /**
     * Display the specified resource.
     *
     * @param $key
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param int $id
     */
    public function show($key)
    {
        return view('key.show')->with('pdfs',KeyCreator::where('key',$key)->first()->pdfs);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
       $request->user()->keys()->where('id',$id)->delete();
       return back()->with('message','Key has been successfully deleted');
    }
}
