<?php

namespace App\Http\Requests;


use App\Key;
use Dingo\Api\Http\FormRequest;

class CreatePdf extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Key::where('key',$this->route('key'))->first();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
