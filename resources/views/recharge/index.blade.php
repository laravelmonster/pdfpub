@extends('recharge.core')
@section('content')
    <div class="pricing-wrapper clearfix">
        <div class="pricing-table">
            <h3 class="pricing-title" style="background: url(https://esewa.com.np/images/body_bg.png);">Esewa (Add Color)</h3>
            <div class="price">
                <img src="https://esewa.com.np/images/logo.png" alt="Esewa" class="img-responsive"></div>
            <ul class="table-list">
                <li>Rs 1 per pdf</li>
                <li>Occasional boost</li>
                <li>Minimum 50</li>
                <li>Quota will be added within 72 Hour.</li>
                <li>Surprise Bonus every month</li>
                <li>Nepali kind of support</li>
            </ul>
        </div>
        <div class="pricing-table recommended">
            <h3 class="pricing-title" style="background: #222d65;" ">PayPal</h3>
            <div class="price"><img src="https://www.paypalobjects.com/webstatic/i/logo/rebrand/ppcom.svg"
                                    class="img-responsive" alt="PayPal"></div>
            <ul class="table-list">
                <li>$0.01 per pdf</li>
                <li>Occasional boost</li>
                <li>Minimum 1$</li>
                <li>Quota will be added within 72 Hour.</li>
                <li>Surprise Bonus every month</li>
                <li><form action="https://www.paypal.com/cgi-bin/webscr" method="post">
                        <input type="hidden" name="cmzd" value="_cart">
                        <input type="hidden" name="business" value="laravelmonster@gmail.com">
                        <input type="hidden" name="item_name" value="PDF API of PDFPUB.com">
                        <input type="hidden" name="item_number" value="1">
                        <input type="hidden" name="amount" value="1">
                        <input type="hidden" name="first_name" value="Suman">
                        <input type="hidden" name="last_name" value="Shrestha">
                        <input type="hidden" name="zip" value="4700">
                        <input type="hidden" name="email" value="summonshr@gmail.com">
                        <input type="image" name="submit"
                               src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif"
                               alt="PayPal - The safer, easier way to pay online">
                    </form></li>
            </ul>
        </div>

        <div class="pricing-table">
            <h3 class="pricing-title" style="background:#ff4800;">Payoneer</h3>
            <div class="price"><img src="https://www.payoneer.com/wp-content/uploads/2015/02/logo.png"
                                    alt="PayPal" class="img-responsive"></div>
            <ul class="table-list">
                <li>$0.01 per pdf</li>
                <li>Occasional boost</li>
                <li>Minimum 1$</li>
                <li>Quota will be added within 72 Hour.</li>
                <li>Surprise Bonus every month</li>
                <li>Support over email</li>
            </ul>
        </div>
    </div>
@endsection