@if(session('message'))
    <div class="row justify-content-center">
        <div class="card card-{{$type ?? session('type') ?? 'success'}} text-center z-depth-2 mb-1">
            <div class="card-block">
                <p class="white-text mb-0">{{session('message')}}</p>
            </div>
        </div>
    </div>
@endif