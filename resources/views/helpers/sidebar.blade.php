<div id="sidebar-wrapper">
    <ul class="sidebar-nav ">
        <li class="sidebar-brand ">
            <a href="/" class="text-danger text-center">
                Administrator
            </a>
        </li>
        <li class="{{active(url('/'))}}">
            <a class="text-center  {{active(url('/'))}}" href="/">Dashboard</a>
        </li>
        <li class="{{active(route('key.index'))}}">
            <a class="text-center {{active(route('key.index'))}}" href="{{route('key.index')}}">Keys</a>
        </li>
        <li class="{{active(route('change-password'))}}">
            <a class="text-center {{active(route('change-password'))}}" href="{{route('change-password')}}">Change
                Password</a>
        </li>
        <li class="{{active(route('digest'))}}">
            <a class="text-center {{active(route('digest'))}}" href="{{route('digest')}}">How to digest ?</a>
        </li>
        <li class="small {{active(route('logout'))}}">
            <a class="text-center {{active(route('logout'))}}" href="{{route('logout')}}">Log Out</a>
        </li>
    </ul>
</div>