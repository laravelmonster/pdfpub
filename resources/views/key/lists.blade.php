@extends('key.core')

@section('content')
    @if(auth()->user()->keys()->count() > 0)
       @include("key.table")
    @else
        <div class="col-sm-12">
            <div class="card card-success text-center z-depth-2 mb-1">
                <div class="card-block">
                    <p class="white-text mb-0">
            You have no keys. Please create one <a href="{{ route('key.create') }}">here</a>.
                    </p>
                </div>
            </div>
        </div>
    @endif

@endsection

