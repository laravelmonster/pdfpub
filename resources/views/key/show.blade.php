@extends('key.core')

@section('content')
    @if($pdfs->count() > 0)
        <div class="row justify-content-center ">
          <div class="col-sm-10">
              <table class="table">
                  <thead class="thead-inverse">
                  <th>ID</th>
                  <th>Name</th>
                  <th>Created at</th>
                  <th>Actions</th>
                  </thead>
                  <tbody>
                  @foreach($pdfs as $pdf)
                      <tr>
                          <td>{{$pdf->id}}</td>
                          <td>{{$pdf->name}}</td>
                          <td title="{{$pdf->created_at}}">{{$pdf->created_at->toFormattedDateString()}}</td>
                          <td>
                              <a target="_blank" class="btn btn-info"
                                 href="{{route('pdf.show',$pdf)}}?type=print"><i class="fa fa-cloud-download"></i>
                                  <strong>View</strong></a>
                              <a class="btn btn-success" href="{{route('pdf.show',$pdf)}}?type=download"><i
                                          class="fa fa-cloud-download"></i>
                                  <strong>Download</strong></a>
                              <form class="d-inline-block" method="post"
                                    action="{{route('pdf.destroy',$pdf)}}">{{csrf_field()}}{{method_field('delete')}}
                                  <input type="hidden" value="{{$pdf->id}}">
                                  <button type="submit" class="btn btn-danger"> Delete</button>
                              </form>
                      </tr>
                  @endforeach
                  </tbody>
              </table>
          </div>
        </div>
    @else
        <div class="col-sm-12">
            <div class="card card-success text-center z-depth-2 mb-1">
                <div class="card-block">
                    <p class="white-text mb-0">
                        No PDF has been saved with this api key.
                    </p>
                </div>
            </div>
        </div>
    @endif
@endsection