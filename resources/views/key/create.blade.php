@extends('key.core')

@section('content')
    <div class="row justify-content-center">
        <div class="col-lg-6 col-md-8">
            <form action="{{ route('key.store') }}" method="post">
                <div class="card">
                    <div class="card-block">
                        <div class="form-header  purple darken-4">
                            <h3><i class="fa fa-lock"></i> Create Key:</h3>
                        </div>
                        {{csrf_field()}}
                        <div class="md-form">
                            <i class="fa fa-lock prefix"></i>
                            <input class="form-control" type="password" name="password"
                                   required/>
                            <label for="form4">Password</label>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-deep-purple"> Log In</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection