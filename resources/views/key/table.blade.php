<div class="container">
    <div style="margin: 0 auto;">
        <a href="{{route('key.create')}}" class="btn btn-blue-grey pull-right">Create</a>
        <table class="table table-striped">
            <thead class="thead-inverse">
            <th class="text-capitalize">key id</th>
            <th class="text-capitalize">key</th>
            <th class="text-capitalize">created at</th>
            <th class="text-capitalize">count of pdf</th>
            <th class="text-capitalize">actions</th>
            </thead>
            <tbody>
            @foreach(auth()->user()->keys()->withCount('pdfs')->get() as $key)
                <tr class="">
                    <td class="align-middle">{{ $key->id }}</td>
                    <td class="key-holder align-middle">{{ $key->key }}
                    </td>
                    <td class="align-middle" title="{{ $key->created_at }}">{{ $key->created_at->toFormattedDateString() }}</td>
                    <td class="align-middle">{{$key->pdfs_count}}</td>
                    <td>
                        <a title="It will show you list of pdf created by this key (only cached)" href="{{ route('key.show',$key->key) }}" class="btn btn-info">View</a>
                        <form class="d-inline-block" action="{{ route('key.destroy',$key) }}"
                              method="POST">{{ csrf_field() }}{{ method_field('delete') }}
                            <button title="Be careful" class="btn btn-danger button" type="submit">
                                <strong>Destroy</strong></button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>