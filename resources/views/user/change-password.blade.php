@extends('template')

@section('content')

    <form action="{{ route('change-password') }}" method="post">
        <div class="row">
            <div class="col-lg-6 col-md-6" style="margin: 0 auto; margin-top:80px;">
                <div class="card">
                    <div class="card-block">
                        <div class="form-header  purple darken-4">
                            <h3><i class="fa fa-lock"></i> Change Password:</h3>
                        </div>
                        {{csrf_field()}}
                        <div class="md-form">
                            <i class="fa fa-lock prefix"></i>
                            <input class="form-control" type="password" name="old_password" required/>
                            <label for="form2">Old Password</label>
                            @if($errors->has('old_password'))<span class="text-danger ml-5">{{$errors->first('old_password')}}</span> @endif
                        </div>
                        <div class="md-form">
                            <i class="fa fa-lock prefix"></i>
                            <input class="form-control" type="password" name="password" required/>
                            <label for="form2">New Password</label>
                            @if($errors->has('password'))<span class="text-danger ml-5">{{$errors->first('password')}}</span> @endif

                        </div>
                        <div class="md-form">
                            <i class="fa fa-lock prefix"></i>
                            <input class="form-control" type="password" name="password_confirmation"
                                   required/>
                            <label for="form4">New Password Confirmation</label>
                            @if($errors->has('password_confirmation'))<span class="text-danger ml-5">{{$errors->first('password_confirmation')}}</span> @endif
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-deep-purple">Change</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
