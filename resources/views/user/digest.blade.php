@extends('template')

@section('content')
    <div class="container justify-content-center">
        <!--Accordion wrapper-->
        <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="card">
                <div class="card-header" role="tab" id="headingOne">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true"
                       aria-controls="collapseOne">
                        <h5 class="mb-0">
                            #1 How to use pufpub.com ? <i class="fa fa-angle-down rotate-icon"></i>
                        </h5>
                    </a>
                </div>
                <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                    <div class="card-block">
                        <p>PDFPUB.com is a straight forward conversion as a service (CAAS) offered by me (Suman
                            Shrestha).
                        </p>
                        <p>As a developer, I was not able to get certain level of freedom because of
                            restriction in shared hosting. So, I started creating tools that I wish existed. This is one
                            of them.</p>
                        <p>This is an online tool to convert your html files, urls, images or doc / word files
                            to pdf. I have set up my hosting to support simple process that you will require in your
                            development process.
                        </p>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" role="tab" id="headingTwo">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"
                       aria-expanded="false" aria-controls="collapseTwo">
                        <h5 class="mb-0">
                            #2 How to recharge ? <i class="fa fa-angle-down rotate-icon"></i>
                        </h5>
                    </a>
                </div>
                <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="card-block">
                        Have no intention to make it public.
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" role="tab" id="headingThree">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree"
                       aria-expanded="false" aria-controls="collapseThree">
                        <h5 class="mb-0">
                            #3 How to make requests ? <i class="fa fa-angle-down rotate-icon"></i>
                        </h5>
                    </a>
                </div>
                <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                    <div class="card-block">
                        <p>
                            This is most simple part as I am only concerned about this.
                        </p>
                        <p>
                            All of your request should be pointed to api.pdfpub.com/api/pdf/{key}
                        </p>
                        <p>
                            Here, get key for your dashboard by generating one.
                        </p>
                        <p>
                            Four parameter can be passed.
                        </p>
                        <ul class="list-group">
                            <li class="list-group-item"> 1. url: pdf will be generated from the content of the url.</li>
                            <li class="list-group-item"> 2. html: pdf will be generated from the content of the html.
                            </li>
                            <li class="list-group-item"> 3. doc: pdf will be generated fro the content of the doc.</li>
                        </ul>
                        <p>
                            You can additionally send me two parameters:
                        </p>
                        <ul class="list-group">
                            <li class="list-group-item">1. cache: Set value in your request then, it will cache that
                                request in the server. Next time
                                you request same, you will be served from the cache. Make sure cache is set while
                                requesting
                                again.
                            </li>
                            <li class="list-group-item">2. name: You can set the name of the file while downloading.
                                File name should be unique as it is
                                your identifier of the files.
                            </li>
                        </ul>


                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" role="tab" id="headingHeadingSecure">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseHeadingSecure"
                       aria-expanded="false" aria-controls="collapseHeadingSecure">
                        <h5 class="mb-0">
                            #4 How secure are my files ? <i class="fa fa-angle-down rotate-icon"></i>
                        </h5>
                    </a>
                </div>

                <div id="collapseHeadingSecure" class="collapse" role="tabpanel" aria-labelledby="headingHeadingSecure">
                    <div class="card-block">
                        <p>
                            Well, my part is to keep things simple.
                        </p>
                        <p>I have arranged all of the files to remain in the
                            storage folder under specific key named folders.
                        </p>
                        <p>
                            So your files will be found stored like
                            storage/keyname/filename.pdf. So unless anyone has your key, no one will be able to touch of
                            files. It is not directly accessible from url.
                        </p>

                        <p>
                            As for the part of privacy, I can assure you that nothing will occur from my side. I am just
                            a
                            developer trying to make life easier. My focus is to make applications softer. I do not have
                            any
                            back end system to handle any task. If you ever make payments, I will manually log into my
                            server and add your quota under your username in mysql.
                        </p>

                        <p>I just do not have intention to build anything unnecessary.</p>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" role="tab" id="headingFour">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour"
                       aria-expanded="false" aria-controls="collapseFour">
                        <h5 class="mb-0">
                            #5 I want refund. How do I get my money back? <i class="fa fa-angle-down rotate-icon"></i>
                        </h5>
                    </a>
                </div>

                <div id="collapseFour" class="collapse" role="tabpanel" aria-labelledby="headingFour">
                    <div class="card-block">
                        <p>
                            This is the most easy part.
                        </p>
                        <p>Send me an email which consists of receipt of payment and write few clear reasons why this
                            service didn't meet your requirement (I will try my best to add it.). And don't forget to
                            mention where should I return your payments.</p>

                        <p>
                            In case of nepali citizen, you can ask for funds back through esewa.
                        </p>

                        <p>Note: Don't ask refunds after you have used up all your quota.</p>
                    </div>
                </div>
            </div>
        </div>
        <!--/.Accordion wrapper-->
    </div>
@endsection
