@extends('template')

@section('content')
    @if(auth()->user()->keys->count() == 0)
        <div class="col-sm-12">
            <div class="card card-success text-center z-depth-2 mb-1">
                <div class="card-block">
                    <p class="white-text mb-0">
                        You have not api keys yet. Create one <a href="{{ route('key.create') }}">here</a>.
                    </p>
                </div>
            </div>
        </div>
    @endif
    @unless(auth()->user()->keys->count() == 0)
        @include('key.table')
    @endunless
@endsection