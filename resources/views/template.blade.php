<!DOCTYPE html>
<html lang="en">

<head>
    @yield('script')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>PDFPUB.com - Making PDF conversion simpler</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/mdb.min.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
</head>

<body>
<div class="container-fluid">
    <div class="row">
        <div id="wrapper">
            <div class="col-sm-2">
                @include('helpers.sidebar')
            </div>
            <div class="col-sm-10 d-inline">
                <div class="col-sm-12">
                    <a href="#menu-toggle" class="btn btn-default" id="menu-toggle" title="Toggle Menu"><i
                                class="fa fa-bars"></i></a>
                    <a href="{{route('recharge')}}" class="pull-right btn btn-brown">Remaining
                        Quota: {{auth()->user()->quota->quota}}</a>
                </div>
                <div class="clearfix"></div>
                @include('helpers.notifier')
                @yield('content')
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="/js/jquery-3.1.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="/js/tether.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="/js/mdb.min.js"></script>

<script defer>
    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

    $('label').click(function () {
        $(this).addClass('active')
    })
</script>
</body>
</html>
