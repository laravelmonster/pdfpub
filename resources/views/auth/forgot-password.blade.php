@extends('auth.auth')

@section("content")
    <form action="{{ route('login') }}" method="post">
        <div class="row">
            <div class="col-lg-4 col-md-6" style="margin: 0 auto; margin-top:80px;">
                <div class="card">
                    <div class="card-block">
                        <div class="form-header  purple darken-4">
                            <h3><i class="fa fa-lock"></i> Login:</h3>
                        </div>
                        {{csrf_field()}}
                        <div class="md-form">
                            <i class="fa fa-envelope prefix"></i>
                            <input class="form-control" type="text" name="username" required/>

                            <label for="form2">Username</label>
                        </div>
                        <div class="md-form">
                            <i class="fa fa-lock prefix"></i>
                            <input class="form-control" type="password" name="password"
                                   required/>
                            <label for="form4">Password</label>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-deep-purple"> Log In</button>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="options">
                            <p>Not a member? <a href="{{ route('user.create') }}">Sign Up</a></p>
                            <p>Forgot <a href="#">Password?</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection