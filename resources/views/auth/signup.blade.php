@extends('auth.auth')

@section("content")
    <div class="col-lg-6 col-md-8" style="margin: 0 auto; margin-top: 80px;">
        <form action="{{ route('user.store') }}" method="post">
            {{csrf_field()}}
            <div class="card">
                <div class="card-block">
                    <div class="form-header blue-gradient">
                        <h3><i class="fa fa-user"></i> Register:</h3>
                    </div>
                    <div class="md-form">
                        <i class="fa fa-user prefix"></i>
                        <input type="text" value="{{old('username')}}" class="form-control" name="username" required>
                        <label for="form3" class="">Username</label>
                        @if($errors->has('username'))<span class="text-danger m-5">{{$errors->first('username')}}</span>@endif
                    </div>
                    <div class="md-form">
                        <i class="fa fa-envelope prefix"></i>
                        <input type="text" value="{{old('email')}}" class="form-control" name="email" required>
                        <label for="form2" class="">Your email</label>
                        @if($errors->has('email'))<span class="text-danger m-5">{{$errors->first('email')}}</span>@endif
                    </div>
                    <div class="md-form">
                        <i class="fa fa-lock prefix"></i>
                        <input type="password" class="form-control" name="password" required>
                        <label for="form4">Your password</label>
                        @if($errors->has('password'))<span class="text-danger m-5">{{$errors->first('password')}} </span>@endif
                    </div>
                    <div class="md-form">
                        <i class="fa fa-lock prefix"></i>
                        <input type="password" class="form-control" name="password_confirmation">
                        <label for="form4">Confirm password</label>
                        @if($errors->has('password_confirmation'))<span class="text-danger m-5">{{$errors->first('password_confirmation')}}@endif
                    </span>
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-indigo waves-effect waves-light"> Sign Up</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection



