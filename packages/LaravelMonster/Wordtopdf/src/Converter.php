<?php

namespace LaravelMonster\Wordtopdf;

use Response;

class Converter
{
    private $path;

    /**
     * Create a new Skeleton Instance
     */
    public function __construct()
    {
        // constructor body
    }

    public function convert($request, $name)
    {
        $store = $name;
        $uploadedFile = request()->file('doc');
        $uploadedFile->storePubliclyAs($request->route('key'), $store . '.' . $uploadedFile->extension());
        $nameWithoutExtension = 'app/' . $request->route('key') . '/' . $store;
        $storage_path = storage_path($nameWithoutExtension . '.' . $uploadedFile->extension());
        shell_exec("unoconv -f pdf " . $storage_path);
        $convertedPath = storage_path($nameWithoutExtension . '.pdf');
        if (\File::exists($convertedPath)) {
            $this->path = $convertedPath;
            return $this;
        }
        return false;
    }

    public function save(){

    }



    public function download($filename = 'output.pdf')
    {
        return Response::download($this->path, $filename, array(
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'attachment; filename="' . $filename . '"'
        ));
    }

    /**
     * Friendly welcome
     *
     * @param string $phrase Phrase to return
     *
     * @return string Returns the phrase passed in
     */
    public function echoPhrase($phrase)
    {
        return $phrase;
    }
}
